import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import HomeView from "@/views/HomeView.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "home",
    component: HomeView,
    
  },
  {
    path: "/home",
    name: "home",
    component: HomeView,
  },
  {
    path: "/about1",
    name: "about1",
    component: () =>
      import("@/views/AboutView_1.vue"),
  },
  {
    path: "/about2",
    name: "about2",
    component: () =>
      import("@/views/AboutView_2.vue"),
  },
  {
    path: "/about3",
    name: "about3",
    component: () =>
      import("@/views/AboutView_3.vue"),
  },
  {
    path: "/educative_offer",
    name: "educative_offer",
    component: () =>
      import("@/views/EducativeOffertView_1.vue"),
  },
  {
    path: "/educative_offer2",
    name: "educative_offer2",
    component: () =>
      import("@/views/EducativeOffertView_2.vue"),
  },
  {
    path: "/educative_offer3",
    name: "educative_offer3",
    component: () =>
      import("@/views/EducativeOffertView_3.vue"),
  },
  {
    path: "/educative_offer4",
    name: "educative_offer4",
    component: () =>
      import("@/views/EducativeOffertView_4.vue"),
  }, 
  {
    path: "/educative_offer5",
    name: "educative_offer5",
    component: () =>
      import("@/views/EducativeOffertView_5.vue"),
  }, 
  {
    path: "/educative_offer6",
    name: "educative_offer6",
    component: () =>
      import("@/views/EducativeOffertView_6.vue"),
  },    
  {
    path: "/educative_offer7",
    name: "educative_offer7",
    component: () =>
      import("@/views/EducativeOffertView_7.vue"),
  },
  {
    path: "/educative_offer8",
    name: "educative_offer8",
    component: () =>
      import("@/views/EducativeOffertView_8.vue"),
  },    
  {
    path: "/partners",
    name: "partners",
    component: () =>
      import("@/views/PartnersView.vue"),
  },
  {
    path: "/professionals",
    name: "professionals",
    component: () =>
      import("@/views/ProfessionalsView.vue"),
  },
  {
    path: "/portal",
    name: "portal",
    component: () =>
      import("@/views/PortalView.vue"),
  },
  {
    path: "/blog",
    name: "blog",
    component: () =>
      import("@/views/BlogView.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior () {     return { x: 0, y: 0 }   }
});

export default router;
